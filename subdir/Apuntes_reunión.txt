1) Temas

9 y 10 de febrero.  Introducción a ROS e instalación, por Paloma de la Puente.
11,  16 y 17 de febrero.  Comunicaciones, conceptos principales, por Paloma de la Puente.

Propuesta:
10 feb (instalación) y 16 de feb(Comunicaciones, conceptos principales)

Introducción (30min):
*Visualización y análisis de los topics que se publican desde el firmware del robot
¿Que topics hay $rostopic list?¿Qué tipos de mensajes manejan $rostopic info?¿Como es la estructura de estos mensajes $rosmsg show?
Uso de los comandos: $rostopic echo, $rostopic pub

Solo Ejercicios(1h y 30min):
*Configurar un nodo
*Publicar y suscribir topics
*Mensajes personalizados



2) Diapositivas y/o documento en inglés?.